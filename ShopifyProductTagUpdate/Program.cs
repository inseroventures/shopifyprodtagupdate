﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using ShopifySharp;
using ShopifySharp.Filters;

namespace ShopifyProductTagUpdate
{
    class Program
    {
        public static IConfigurationRoot _config { get; set; }
        static ProductService _pService;

        static async void LoopProducts()
        {

            var currentPage = 1;
            var cnt = 0;
            bool complete = false;
            _pService = new ProductService(_config["Shopify:CustomerWebsite"], _config["Shopify:CustomerPassword"]);

            while (!complete)
            {

                System.Threading.Thread.Sleep(1000);

                var products = _pService.ListAsync(new ProductFilter() { Page = currentPage, Limit = 250 }).Result.ToList();

                foreach (var prod in products)
                {

                    Console.Write(prod.Title);

                    //             if (String.Compare(prod.Title, "Specialized Enduro XL Locking Grip") <= 0) continue;

                    string tags = prod.Tags;

                    
               
                    if (!tags.ToLower().Contains(prod.Vendor.ToLower()))
                        tags = tags + "," + prod.Vendor;
                    else
                    {
                        Console.WriteLine("... Skipping");
                        continue;
                    }

                    if (!tags.ToLower().Contains(prod.ProductType.ToLower()))
                        tags = tags + "," + prod.ProductType;

                    var retPrice = prod.Variants.First().Price;

                    if (retPrice <= 100)
                    {
                        if (!tags.Contains("0-100")) tags = tags + ",0-100";
                    }
                    else if (retPrice <= 300)
                    {
                        if (!tags.Contains("100-300")) tags = tags + ",100-300";
                    }
                    else if (retPrice < 1000)
                    {
                        if (!tags.Contains("300-1000")) tags = tags + ",300-1000";
                    }
                    else if (retPrice < 2000)
                    {
                        if (!tags.Contains("1000-2000")) tags = tags + ",1000-2000";
                    }
                    else if (retPrice < 5000)
                    {
                        if (!tags.Contains("2000-5000")) tags = tags + ",2000-5000";
                    }
                    else if (retPrice < 10000)
                    {
                        if (!tags.Contains("5000-10000")) tags = tags + ",5000-10000";
                    }
                    else if (retPrice < 20000)
                    {
                        if (!tags.Contains("10000-20000")) tags = tags + ",10000-20000";
                    }
                    else
                    {
                        if (!tags.Contains("20000-Max")) tags = tags + ",20000-Max";

                    }

                 //   bool hasInvent = false;
                    DateTimeOffset? pAt = null;
                    foreach (var v in prod.Variants)
                    {
                        if (v.InventoryManagement == "shopify" &&  v.InventoryQuantity > 0)
                        {
                            pAt = prod.PublishedAt;
                            break;
                        }


                    }

                    System.Threading.Thread.Sleep(500);
                    try
                    {
                        var p = await _pService.UpdateAsync((long)prod.Id, new Product()
                        {
                            Tags = tags,
                            PublishedAt = pAt,
                            PublishedScope = "global"
                        });
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Problem with save - check output" + ex.Message);
                    }

                    Console.WriteLine("... Updated");


                }

                complete = !products.Any();
                currentPage = currentPage + 1;
            }

            return;
        }


        static void Main(string[] args)
        {

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            //   .AddEnvironmentVariables();


            _config = builder.Build();
            Console.WriteLine($"Website = {_config["Shopify:CustomerWebsite"]}");
            Console.WriteLine($"Password = {_config["Shopify:CustomerPassword"]}");

            

            LoopProducts();
            Console.WriteLine("All complete");
               Console.ReadLine();



        }
    }
}
